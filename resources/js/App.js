import React, {Component} from 'react';
import axios from 'axios';
import swal from 'sweetalert2';



class App extends Component {
  constructor() {

  super();
        this.state = {
          nombre: '',
          email: '',
          telefono: '',
        };
      }
      onChange = (e) => {
      
        this.setState({ [e.target.name]: e.target.value });
      }
      onSubmit = (e) => {
        e.preventDefault();
        const { nombre, email,telefono } = this.state;

        axios.post('/api/lead', { nombre, email, telefono })
          .then((res) => {
            //access the results here....
            if(res.data.status === 200)
            {
                //swal("Success!",res.data.message,"success");
                alert("datos Guardados");
                this.setState({
                    nombre: '',
                    email: '',
                    telefono: '',
                    error_list: [],
                });
                
            }
            else if(res.data.status === 422)
            {
              this.setState({ error_list: res.data.validate_err });
            }
        
          });
      }
          
      render() {
        const { nombre, email,telefono } = this.state;
  return (
    <form onSubmit={this.onSubmit}>

      <div className="p">
        <label>
          <span className="ui-label">
            Nombre
          </span>
          <br/>
          <input type="text" className="inputtext full" placeholder="Nombre y apellidos" name="nombre" value={nombre}
              onChange={this.onChange}/>
        </label>
      </div>
      <div className="p">
        <label>
          <span className="ui-label">
            Email
          </span>
          <br/>
          <input type="email" className="inputtext full" placeholder="ejemplo@empresa.com" name="email" value={email}
              onChange={this.onChange} />
        </label>
      </div>
      <div className="p">
        <label>
          <span className="ui-label">
            Teléfono
          </span>
          <br/>
          <input type="tel" className="inputtext full" placeholder="10 dígitos" name="telefono" value={telefono}
              onChange={this.onChange} />
        </label>
      </div>
      <div className="p">
        <button className="button" type="submit" value="submit">Enviar</button>
      </div>
    </form>
  );
}
}

export default App;
